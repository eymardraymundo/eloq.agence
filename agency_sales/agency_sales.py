# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2004-2010 Tiny SPRL (<http://tiny.be>).
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################
from osv import fields, osv
from tools.translate import _
import time
import tools
from mx import DateTime
import datetime
from dateutil.relativedelta import relativedelta
import decimal_precision as dp
import netsvc
import pooler
from datetime import datetime, date
#from tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT, float_compare

class product_category(osv.osv):
    _inherit = "product.category"
    _name = "product.category"
    _columns = {
        'type': fields.selection([('ticket','Ticket'), ('room','Room'), ('act','Activity'), ('packet','Packet')], 'Category Type', required=True),
    }


    _defaults = {
        'type' : lambda *a : 'ticket',
    }

product_category()

class elo_flight_line(osv.osv):
    _name = "elo.flight.line"

    _columns = {
        'name': fields.char('Name', size=256, required=True),
        'description': fields.text('Description'),
        }

elo_flight_line()



class elo_origin_destiny(osv.osv):
    _name = "elo.origin.destiny"

    def name_get(self, cr, uid, ids, context=None):
        if not len(ids):
            return []
        destiny = self.browse(cr, uid, ids, context=context)
        res = []
        for record in destiny:
            name = record.name +' / '+ record.state_id.name +' / '+ record.country_id.name
            res +=[(record.id, name)]
        return res

    _columns = {
        'name': fields.char('Origin or Destiny', size=64, required=True, translate=True, select=True),
        'country_id': fields.many2one('res.country', 'Country',
            required=True),
        'state_id': fields.many2one('res.country.state', 'Country State',
            required=True),

        }

elo_origin_destiny()


class elo_passenger(osv.osv):
    _name = "elo.passenger"
    _descrition = "Passenger"

    def _get_age(self, cr, uid, ids, name, arg, context=None):
        res = {}
        if not ids:
            return res
        for obj in self.browse(cr, uid, ids, context):
            if obj.birthdate:
                delta = date.today() - datetime.strptime(obj.birthdate, '%Y-%m-%d').date()
                age = date.fromordinal(delta.days).year - 1
                res[obj.id] = age
            else:
                res[obj.id] = 0
        return res

    def get_age(self, cr, uid, ids, birthdate=None):
        res = {}
        if birthdate:
            try:
                delta = date.today() - datetime.strptime(birthdate, '%Y-%m-%d').date()
                age = date.fromordinal(delta.days).year - 1
                res.update({'age': age })
                return {'value' : res}
            except:
                pass
        return {'value':{'age':0}}

    _columns = {
        'name':fields.char('Name', size=64, required=True, select=True),
        'gender':fields.selection([('female', 'Female'), ('male', 'Male')], 'Gender', select=True),
        'age':fields.function(_get_age, type="integer", string='Age'),
        'birthdate':fields.date('Birthdate'),
        'passport':fields.char('No. passport', size=64, select=True),
        'country_id': fields.many2one('res.country','Nationality', select=True),
        }

elo_passenger()

class elo_hotel(osv.osv):
    _name = "elo.hotel"

    _columns = {
        'name': fields.char('Name', size=256, required=True),
        'description': fields.text('Description'),
        'country_id': fields.many2one('res.country', 'Country',
            required=True),
        'state_id': fields.many2one('res.country.state', 'Country State',
            required=True),
        }

elo_hotel()


class sale_order_line(osv.osv):
    _name = "sale.order.line"
    _inherit = "sale.order.line"

    _columns = {
        'categ_id': fields.many2one('product.category','Category', required=True, readonly=True, change_default=True,
            states={'draft': [('readonly', False)]},help="Select category for the current product"),
        'type_categ': fields.char('Type Category',size=16),
        #Data of product ticket
        'date': fields.datetime('Date and Hour', readonly=True, states={'draft': [('readonly', False)]}),
        'flight_line': fields.many2one('elo.flight.line','Flight Line', readonly=True, states={'draft': [('readonly', False)]}),
        'destiny': fields.many2one('elo.origin.destiny','Destiny', readonly=True, states={'draft': [('readonly', False)]}),
        'origin': fields.many2one('elo.origin.destiny','Origin', readonly=True, states={'draft': [('readonly', False)]}),
        'number_flight': fields.char('No. Flight', size=16),
        'seat': fields.char('Seat', size=16),
        'number_ticket': fields.char('No. Ticket', size=16),
        'passenger_id': fields.many2one('elo.passenger','Passenger', readonly=True, states={'draft': [('readonly', False)]}),
        #Data of product room
        'check_out': fields.datetime('CheckOut', readonly=True, states={'draft': [('readonly', False)]}),
        'check_in': fields.datetime('CheckIn', readonly=True, states={'draft': [('readonly', False)]}),
        'hotel_id':  fields.many2one('elo.hotel','Hotel', readonly=True, states={'draft': [('readonly', False)]}),
        'passengers': fields.many2many('elo.passenger','elo_order_line_passenger','order_line_id','passenger_id','Passenger', readonly=True, states={'draft': [('readonly', False)]}),




    }

    def onchange_categ(self, cr, uid, ids, categ_id, context=None):
        if categ_id:
            categ = self.pool.get('product.category').browse(cr, uid, categ_id,context)
            return {'value': {'type_categ':categ.type}}
        return {'value': {'type_categ':''}}



sale_order_line()




